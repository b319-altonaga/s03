const chai = require("chai");

const { expect } = chai;

const chaiHttp = require("chai-http");

chai.use(chaiHttp);

describe("api_test_suite", () => {
  it("test_api_get_people_is_running", () => {
    chai
      .request("http://localhost:3000")
      .get("/people")
      .end((err, res) => {
        expect(res.body).to.not.equal(undefined);
      });
  });

  // Check if api response is 200
  it("test_api_get_people_returns_200", (done) => {
    chai
      .request("http://localhost:3000")
      .get("/people")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done(); // used in async tests to indicate that the test is complete
      });
  });

  // Test for /PERSON POST ROUTE
  it("test_api_post_person_returns_if_person_no_name", (done) => {
    chai
      .request("http://localhost:3000")
      .post("/person")
      .send({ alias: "test", age: 20 })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done(); // used in async tests to indicate that the test is complete
      });
  });

  it("test_api_post_person_returns_if_age_is_string", (done) => {
    chai
      .request("http://localhost:3000")
      .post("/person")
      .send({ name: "James", age: "Twenty-Eight" })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done(); // used in async tests to indicate that the test is complete
      });
  });

  it("test_api_post_person_is_running", (done) => {
    chai
      .request("http://localhost:3000")
      .post("/person")
      .send({ name: "James", age: 28 })
      .end((err, res) => {
        expect(res.body).to.not.equal(undefined);
        done(); // used in async tests to indicate that the test is complete
      });
  });

  it("test_api_post_person_returns_400_if_no_ALIAS", (done) => {
    chai
      .request("http://localhost:3000")
      .post("/person")
      .send({ name: "James", age: 28, alias: null })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done(); // used in async tests to indicate that the test is complete
      });
  });

  it("test_api_post_person_returns_400_if_no_AGE", (done) => {
    chai
      .request("http://localhost:3000")
      .post("/person")
      .send({ name: "James", age: null, alias: "James" })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done(); // used in async tests to indicate that the test is complete
      });
  });
});
