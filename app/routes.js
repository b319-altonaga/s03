const { names } = require("../src/util");

module.exports = (app) => {
  app.get("/", (req, res) => {
    res.send("Hello World!");
  });

  app.get("/people", (req, res) => {
    return res.json({ people: names });
  });

  app.post("/person", (req, res) => {
    if (!req.body.hasOwnProperty("name")) {
      return res.status(400).json({ error: "Missing name" });
    }

    if (typeof req.body.name !== "string") {
      return res.status(400).json({ error: "Name must be a string" });
    }

    if (!req.body.hasOwnProperty("age")) {
      return res.status(400).json({ error: "Missing age" });
    }

    if (typeof req.body.age !== "number") {
      return res.status(400).json({ error: "Age must be a number" });
    }

    if (!req.body.hasOwnProperty("username")) {
      return res.status(400).json({ error: "Missing username" });
    }

    if (typeof req.body.username !== "string") {
      return res.status(400).json({ error: "Username must be a string" });
    }
  });
};
