const express = require("express");

const app = express();

app.use(express.json());

require("./app/routes")(app, {});

app.listen(3000, () => {
  console.log("Running on port 3000");
});
